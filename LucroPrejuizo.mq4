//+------------------------------------------------------------------+
//|                                                LucroPrejuizo.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.facebook.com/vitorjosecezar.santos"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   double resultado = 0;
   for(int i=0;i< OrdersTotal();i++){
      if(OrderSelect(i,SELECT_BY_POS)){
         resultado += OrderProfit();
      }
      else{
         Print("Falha ao selecionar a ordem na posi��o ",i);
      }
   }
   Print("Resultado final ",resultado);
  }
//+------------------------------------------------------------------+
